import multiprocessing

# if current_process().name == 'MainProcess':
from robot import *
from random import randint

from itertools import combinations
from random import shuffle, random
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects

import pickle
import glob

plt.rcParams['figure.dpi'] = 300
plt.rcParams['savefig.dpi'] = 300


# base_filename is the filename (including the path) without the suffix _x.bin
def load_levels(base_filename):
    files = glob.glob(base_filename + '*.bin')

    result = list()
    for filename in files:
        with open(filename, 'rb') as file:
            result.append(pickle.load(file))

    return result


class PlaceKeyProblem(SearchProblem):
    def __init__(self, state: str, goal: str, doors: list[Door]):
        self.initial_state = state
        self.goal = goal
        self.doors = doors

    def possible_actions(self, state):
        actions = list()

        for door in self.doors:
            # new version: don't care about locked doors
            if state in door.goes_between:
                actions.append(("move", door.other_loc[state]))

        return actions

    def successor(self, state, action):
        act, next_state = action
        return next_state

    def goal_test(self, state):
        return state == self.goal

    def display_state(self, state):
        pass


def all_doors(rooms_count):
    return [Door('Room ' + str(x[0]), 'Room ' + str(x[1])) for x in combinations(range(rooms_count), 2)]


def find_doors_for_room(room, doors, ignored_doors=list()):
    return [x for x in doors if room in x.goes_between and x not in ignored_doors]


def traverse_random(start_room, doors, n, doors_not_to_use):
    result = start_room
    locked_doors_bypassed = list()
    went_through = list()  # ensures no going backwards
    for _ in range(n):
        choices = find_doors_for_room(result, doors, doors_not_to_use + went_through)
        locked_choices = [x for x in choices if x.locked]
        if len(choices) > 0:
            # prioritize going through locked doors
            choice = choices[randint(0, len(choices) - 1)] if len(locked_choices) == 0 else locked_choices[
                randint(0, len(locked_choices) - 1)]
            went_through.append(choice)
            if choice.locked:
                locked_doors_bypassed.append(choice)
            result = choice.other_loc[result]
        else:
            return result, locked_doors_bypassed

    return result, locked_doors_bypassed


# recursively places keys (at least 1) so that all the rooms locked by these keys can be accessed
# modifies keys_already_placed on the go
def place(key, door, start_room, doors, n, room_contents, keys_already_placed=list(), doors_not_to_use=list()):
    if len(doors_not_to_use) == 0:
        doors_not_to_use = [door]

    room, bypassed_doors = traverse_random(start_room, doors, n, doors_not_to_use)

    if room not in room_contents:
        room_contents[room] = list()
    room_contents[room].append(key)
    keys_already_placed.append(key)

    for bypassed_door in bypassed_doors[::-1]:
        doors_not_to_use.append(bypassed_door)

        if bypassed_door.doorkey in keys_already_placed:
            continue

        place(bypassed_door.doorkey, bypassed_door, start_room, doors, n, room_contents, keys_already_placed,
              doors_not_to_use)


def generate(robot_capacity, rooms_count, additional_doors_count, locked_doors_count,
             goal_items_count, useless_items_count, max_item_weight, visualise=False, save=True, file_name='level'):
    robot = Robot('Room 0', [], robot_capacity)

    room_contents = dict()
    item_weights = dict()
    goal_item_locations = dict()

    # create empty rooms
    for i in range(rooms_count):
        room_contents['Room ' + str(i)] = list()

    # add useless items to rooms
    for i in range(useless_items_count):
        item = 'Useless item ' + str(i)
        item_weights[item] = randint(0, max_item_weight)
        insort(room_contents['Room ' + str(randint(0, rooms_count - 1))], item)

    # we want to have twice as many light items as we do hard for more uniform levels
    light_items_weights = [randint(1, max_item_weight // 2) for _ in range(goal_items_count * 2)]
    heavy_items_weights = [randint(max_item_weight // 2 + 1, max_item_weight) for _ in range(goal_items_count)]
    weights = light_items_weights + heavy_items_weights
    shuffle(weights)

    # add goal items to rooms
    for i in range(goal_items_count):
        item = 'Goal item ' + str(i)
        item_weights[item] = weights[i]
        insort(room_contents['Room ' + str(randint(0, rooms_count - 1))], item)

        randgoal = 'Room ' + str(randint(0, rooms_count - 1))
        if randgoal not in goal_item_locations or goal_item_locations[randgoal] is None:
            goal_item_locations[randgoal] = list()
        insort(goal_item_locations[randgoal], item)

    # create doors
    doors = [Door('Room 1', 'Room 0')]
    for i in range(2, rooms_count):
        doors.append(Door('Room ' + str(i), 'Room ' + str(randint(0, i - 1))))

    additional_doors = [x for x in all_doors(rooms_count) if x not in doors]
    shuffle(additional_doors)
    doors += additional_doors[:min(len(additional_doors), additional_doors_count)]

    # lock the desired number of doors and create appropriate keys
    shuffle(doors)
    keys = dict()
    for i in range(min(len(doors), locked_doors_count)):
        doorkey = 'Key ' + str(i)
        doors[i].locked = True
        doors[i].doorkey = doorkey
        item_weights[doorkey] = 0
        keys[doorkey] = doors[i]

    # place all keys in rooms randomly but the puzzle will still remain solvable
    not_placed = list(keys.keys())
    placed = list()
    while len(not_placed) > 0:
        place(not_placed[0], keys[not_placed[0]], 'Room 0', doors, rooms_count - 1, room_contents, placed)
        not_placed = [x for x in not_placed if x not in placed]

    result = RobotServant(State(robot, doors, room_contents), goal_item_locations, item_weights)

    # matplotlib level preview
    text = list()
    roomsx = [i % round(rooms_count ** 0.5) + (random() - 0.5) * 0.5 for i in range(rooms_count)]
    roomsy = [i // round(rooms_count ** 0.5) + (random() - 0.5) * 0.5 for i in range(rooms_count)]

    fig, ax = plt.subplots()
    ax.scatter(roomsx, roomsy, s=1000, c='teal')
    for room in room_contents:
        room_i = int(room.replace('Room ', ''))
        text.append(ax.text(roomsx[room_i], roomsy[room_i] - 0.1, s=room_i, c='teal'))

    for door in doors:
        for e in door.goes_between:
            break
        room0, room1 = e, door.other_loc[e]
        room0 = room0.replace('Room ', '')
        room1 = room1.replace('Room ', '')
        ax.plot((roomsx[int(room0)], roomsx[int(room1)]),
                (roomsy[int(room0)], roomsy[int(room1)]), c='lightgreen' if not door.locked else 'red')

    for room, contents in room_contents.items():
        room_i = int(room.replace('Room ', ''))

        # first plot useless cause they don't matter
        for item in contents:
            if 'Useless' in item:
                x = roomsx[room_i] + (random() - 0.5) * 0.3
                y = roomsy[room_i] + (random() - 0.5) * 0.3
                ax.scatter(x, y, c='grey', s=100)

        # the plot goal
        for item in contents:
            if 'Goal' in item:
                for goal, goal_content in goal_item_locations.items():
                    if item in goal_content:
                        x = roomsx[room_i] + (random() - 0.5) * 0.3
                        y = roomsy[room_i] + (random() - 0.5) * 0.3
                        ax.scatter(x, y, c='yellowgreen', s=150)
                        text.append(ax.text(x, y, s=goal.replace('Room ', ''), c='yellowgreen',
                                            ha='center', va='center'))
                        break

        # then keys that should always be visible
        for item in contents:
            if 'Key' in item:
                door = [x for x in doors if x.doorkey == item][0]
                for e in door.goes_between:
                    break
                room0, room1 = e, door.other_loc[e]
                x = roomsx[room_i] + (random() - 0.5) * 0.3
                y = roomsy[room_i] + (random() - 0.5) * 0.3
                ax.scatter(x, y, c='gold', s=200)
                text.append(ax.text(x, y, s=room0.replace('Room ', '') + '-' + room1.replace('Room ', ''),
                                    c='gold', ha='center', va='center'))
    #                 text.append(ax.text(x, y + 0.2, s=item, c='black', ha='center', va='center'))

    for t in text:
        t.set_path_effects([path_effects.Stroke(linewidth=0.5, foreground='black'),
                            path_effects.Normal()])

    if save:
        with open(f'levels/{file_name}.bin', 'w+b') as file:
            pickle.dump(result, file)
        plt.savefig(f'levels/{file_name}.png')

    if visualise:
        plt.show()
    plt.close()

    print('Generated', file_name)
    return result


def generate_multi(robot_capacity, rooms_count, additional_doors_count, locked_doors_count, goal_items_count,
                   useless_items_count, max_item_weight, visualise=False, save=True, file_name='level',
                   count=50, count_per_thread=1):
    with multiprocessing.Pool() as p:
        return p.starmap(generate, ((robot_capacity, rooms_count, additional_doors_count, locked_doors_count,
                                     goal_items_count, useless_items_count, max_item_weight, visualise, save,
                                     file_name + f'_{i}') for i in range(count)), count_per_thread)
