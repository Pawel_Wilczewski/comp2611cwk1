from multiprocessing import current_process

if current_process().name == 'MainProcess':
    import os
    from random import seed
    from glob import glob
    from level import generate_multi

    seed(123)

    files = glob('levels/easy*')
    for f in files:
        os.remove(f)

    easy = generate_multi(robot_capacity=25,
                          rooms_count=4,
                          additional_doors_count=0,
                          locked_doors_count=1,
                          goal_items_count=5,
                          useless_items_count=10,
                          max_item_weight=12,
                          visualise=False,
                          save=True,
                          file_name='easy',
                          count=50)

    files = glob('levels/medium*')
    for f in files:
        os.remove(f)

    medium = generate_multi(robot_capacity=25,
                            rooms_count=5,
                            additional_doors_count=2,
                            locked_doors_count=5,
                            goal_items_count=5,
                            useless_items_count=10,
                            max_item_weight=12,
                            visualise=False,
                            save=True,
                            file_name='medium',
                            count=50)

    files = glob('levels/hard*')
    for f in files:
        os.remove(f)

    hard = generate_multi(robot_capacity=25,
                          rooms_count=8,
                          additional_doors_count=2,
                          locked_doors_count=9,
                          goal_items_count=5,
                          useless_items_count=10,
                          max_item_weight=12,
                          visualise=False,
                          save=True,
                          file_name='hard',
                          count=50)

    files = glob('levels/insane*')
    for f in files:
        os.remove(f)

    insane = generate_multi(robot_capacity=35,
                            rooms_count=12,
                            additional_doors_count=3,
                            locked_doors_count=12,
                            goal_items_count=8,
                            useless_items_count=12,
                            max_item_weight=12,
                            visualise=False,
                            save=True,
                            file_name='insane',
                            count=50)
