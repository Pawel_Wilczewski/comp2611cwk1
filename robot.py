from bbmodcache.bbSearch import SearchProblem
from copy import deepcopy
from bisect import insort


class Globals:
    GOAL_ITEM_LOCATIONS = None
    ITEM_WEIGHTS = None
    w = None


class Robot:
    def __init__(self, location, carried_items, strength):
        self.location = location
        self.carried_items = carried_items
        self.strength = strength

    def weight_carried(self):
        return sum([Globals.ITEM_WEIGHTS[i] for i in self.carried_items])


class Door:
    def __init__(self, roomA, roomB, doorkey=None, locked=False):
        self.goes_between = {roomA, roomB}
        self.doorkey = doorkey
        self.locked = locked
        # Define handy dictionary to get room on other side of a door
        self.other_loc = {roomA: roomB, roomB: roomA}

    def __eq__(self, other):
        return self.goes_between == other.goes_between


class State:
    def __init__(self, robot, doors, room_contents):
        self.robot = robot
        self.doors = doors
        self.room_contents = room_contents

    ## Define a string representation that will be uniquely identify the state.
    ## An easy way is to form a tuple of representations of the components of
    ## the state, then form a string from that:
    def __repr__(self):
        return str((self.robot.location, self.robot.carried_items,
                    [d.locked for d in self.doors],
                    self.room_contents))


class RobotServant(SearchProblem):

    def __init__(self, state: State, goal_item_locations, item_weights):
        self.initial_state = state
        self.goal_item_locations = goal_item_locations
        self.item_weights = item_weights

    def possible_actions(self, state):

        robot_location = state.robot.location
        strength = state.robot.strength
        weight_carried = state.robot.weight_carried()

        actions = []
        # Can put down any carried item
        for i in state.robot.carried_items:
            actions.append(("put down", i))

        # Can pick up any item in room if strong enough
        for i in state.room_contents[robot_location]:
            if strength >= weight_carried + Globals.ITEM_WEIGHTS[i]:
                actions.append(("pick up", i))

        # If there is an unlocked door between robot location and
        # another location can move to that location
        for door in state.doors:
            if robot_location in door.goes_between:
                if door.locked:
                    if door.doorkey in state.robot.carried_items:
                        actions.append(("unlock", door))
                else:
                    actions.append(("move to", door.other_loc[robot_location]))

        # Now the actions list should contain all possible actions
        return actions

    def successor(self, state, action):
        next_state = deepcopy(state)
        act, target = action
        if act == "put down":
            next_state.robot.carried_items.remove(target)
            insort(next_state.room_contents[state.robot.location], target)

        if act == "pick up":
            insort(next_state.robot.carried_items, target)
            next_state.room_contents[state.robot.location].remove(target)

        if act == "move to":
            next_state.robot.location = target

        if act == "unlock":
            next_state.doors[next_state.doors.index(target)].locked = False

        return next_state

    def goal_test(self, state):
        # ensure the global item weights and goal locations are up-to-date (this func is called first)
        Globals.GOAL_ITEM_LOCATIONS = self.goal_item_locations
        Globals.ITEM_WEIGHTS = self.item_weights

        for room, contents in Globals.GOAL_ITEM_LOCATIONS.items():
            if contents == None:
                continue

            for i in contents:
                if not i in state.room_contents[room]:
                    return False
        return True

    def display_state(self, state):
        print("Robot location:", state.robot.location)
        print("Robot carrying:", state.robot.carried_items)
        print("Room contents:", state.room_contents)
