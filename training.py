from multiprocessing import current_process
from heuristic import *
from testsuite import test_algorithm

if current_process().name == 'MainProcess':
    from scipy.optimize import minimize
    from numpy import savetxt
    from level import load_levels
    import numpy as np

    train = load_levels('levels/train/train')

    current_best = None


    def test_heuristic(x, *args):
        Globals.w = x
        result = test_algorithm(test_states=train,
                                mode='BF',
                                max_nodes=10000,
                                loop_check=True,
                                randomise=False,
                                cost=cost,
                                heuristic=heuristic_optimized)

        try:
            float(result['solution_efficiency'])
        except:
            return 10 ** 10

        score = result['time'] * result['memory'] * result['solution_efficiency'] ** 2 / result['successful_runs']

        global current_best
        if current_best is None or score < current_best:
            current_best = score
            savetxt('weights.txt', np.array([x]), delimiter=',')

            print('weights:', x)
            print('test outcome:', result)
            print('result:', score)

        return score


    minimize(test_heuristic, Globals.w, method='Nelder-Mead')
