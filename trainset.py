from multiprocessing import current_process

if current_process().name == 'MainProcess':
    import os
    from glob import glob
    from level import generate_multi

    # prepare train set
    files = glob('levels/train/train*')
    for f in files:
        os.remove(f)

    train = generate_multi(robot_capacity=25,
                           rooms_count=6,
                           additional_doors_count=3,
                           locked_doors_count=6,
                           goal_items_count=5,
                           useless_items_count=10,
                           max_item_weight=12,
                           visualise=False,
                           save=True,
                           file_name=f'train/train',
                           count=100)
