from bbmodcache.bbSearch import search
from robot import *
import numpy as np
from numpy import loadtxt

Globals.w = loadtxt('weights.txt', delimiter=',')


def cost(p, s):
    return len(p)


class DistanceProblem(SearchProblem):
    def __init__(self, state: str, goal: str, doors: list[Door], respect_locked=False):
        self.initial_state = state
        self.goal = goal
        self.doors = doors
        self.respect_locked = respect_locked
        self.keys_acquiring_cost = 0

    def possible_actions(self, state):
        actions = list()

        for door in self.doors:
            if state in door.goes_between:
                # TODO: maybee if requires key, first find out the cost of getting the key and sum up these 2
                # watch out for infinite recursion...
                if self.respect_locked and door.locked:
                    #                     result = search(DistanceProblem(state.robot.location, room, state.doors),
                    #                                                     "BF",
                    #                                                     1000,
                    #                                                     loop_check=True,
                    #                                                     randomise=False,
                    #                                                     cost=None,
                    #                                                     heuristic=None,
                    #                                                     show_path=False,
                    #                                                     show_state_path=False,
                    #                                                     dots=False,
                    #                                                     return_info=True,
                    #                                                     print_debug=False)

                    #                 if result['result']['termination_condition'] != 'GOAL_STATE_FOUND':
                    #                     pass # what to do if couldn't find the path?
                    #                 else:
                    #                     self.keys_acquiring_cost += result['result']['path_length']
                    pass

                else:
                    actions.append(("move", door.other_loc[state]))

        return actions

    def successor(self, state, action):
        act, next_state = action
        return next_state

    def goal_test(self, state):
        if state == self.initial_state:
            self.keys_acquiring_cost = 0
        return state == self.goal

    def display_state(self, state):
        pass


# cumulative distance of all the items from their goal rooms (useless items are skipped)
def distance_items(state):
    res = 0
    for goal_room, goal_items in Globals.GOAL_ITEM_LOCATIONS.items():
        for item in goal_items:
            # find the room that the item is currently in
            current_room = None
            if item in state.robot.carried_items:
                current_room = state.robot.location
            else:
                for r, i in state.room_contents.items():
                    if item in i:
                        current_room = r
                        break

            # perform bf search for the path length
            result = search(DistanceProblem(current_room, goal_room, state.doors),
                            "BF",
                            1000,
                            loop_check=True,
                            randomise=False,
                            cost=None,
                            heuristic=None,
                            show_path=False,
                            show_state_path=False,
                            dots=False,
                            return_info=True,
                            print_debug=False)

            # if there's no path, the puzzle is impossible
            if result['result']['termination_condition'] != 'GOAL_STATE_FOUND':
                print("IMPOSSIBLE PUZZLE!!!!! OR TOO COMPLICATED ROOM LAYOUT")
                res += 99999999999
            else:
                res += result['result']['path_length']
            # print(item, 'FROM', current_room, 'TO', goal_room, 'PATH LENGTH:', result['result']['path_length'])

    return res


def distance_robot(state):
    res = 0

    # for all the items in rooms (no need to compute for carried items as it's always 0)
    for room, items in state.room_contents.items():
        # check if item is a goal item
        # TODO: performance improvement if we store goal items in an array
        #       and useless items in another one
        #       these two arrays can be altered using the below code so no big changes necessary
        #       if decided to do that though, remember to alter the code in the heuristic as well
        for item in items:
            is_goal = False
            for goal_room, goal_items in Globals.GOAL_ITEM_LOCATIONS.items():
                # only consider goal items that are not in the goal room yet
                if item in goal_items and room != goal_room:
                    is_goal = True
                    break

            if not is_goal:
                continue

            result = search(DistanceProblem(state.robot.location, room, state.doors),
                            "BF",
                            1000,
                            loop_check=True,
                            randomise=False,
                            cost=None,
                            heuristic=None,
                            show_path=False,
                            show_state_path=False,
                            dots=False,
                            return_info=True,
                            print_debug=False)

            # if there's no path, the puzzle is impossible
            if result['result']['termination_condition'] != 'GOAL_STATE_FOUND':
                print("IMPOSSIBLE PUZZLE!!!!! OR TOO COMPLICATED ROOM LAYOUT")
                res += 99999999999
            else:
                res += result['result']['path_length']
            # print(item, 'FROM ROBOT', state.robot.location, 'TO', room, 'PATH LENGTH:',
#                               result['result']['path_length'])

    return res


def heuristic_optimized(state):
    # big cumulative distance from items to their goal locations is bad
    itmdist = distance_items(state)

    # robot far to incorrectly placed goal items is bad
    robdist = distance_robot(state)

    # carried, 'useless' items are bad
    carried_useless = 0
    for item in state.robot.carried_items:
        useful = False
        for goal_room, goal_items in Globals.GOAL_ITEM_LOCATIONS.items():
            if item in goal_items:
                useful = True
                break
        if not useful:
            carried_useless += 1

    # carried keys for unlocked doors are bad if they weigh anything
    incorrect_keys = 0
    for door in state.doors:
        if not door.locked and door.doorkey in state.robot.carried_items and Globals.ITEM_WEIGHTS[door.doorkey] > 0:
            incorrect_keys += 1

    # locked doors are bad
    locked_doors = sum([1 if door.locked else 0 for door in state.doors])

    # carried items in incorrect rooms are good
    carried_incorrect = 0
    for item in state.robot.carried_items:
        for goal_room, goal_items in Globals.GOAL_ITEM_LOCATIONS.items():
            if item in goal_items and state.robot.location != goal_room:
                carried_incorrect += 1

    # not-carried items in correct rooms are good
    not_carried_correct = 0
    for current_room, current_items in state.room_contents.items():
        for item in current_items:
            if current_room in Globals.GOAL_ITEM_LOCATIONS and item in Globals.GOAL_ITEM_LOCATIONS[current_room]:
                not_carried_correct += 1

    # carried keys for locked doors are good
    correct_keys = 0
    for door in state.doors:
        if door.locked and door.doorkey in state.robot.carried_items:
            correct_keys += 1

    return np.dot(np.array([robdist,
                            itmdist,
                            incorrect_keys,
                            carried_useless,
                            locked_doors,
                            carried_incorrect,
                            not_carried_correct,
                            correct_keys]), np.transpose(Globals.w))
