from multiprocessing import current_process

if current_process().name == 'MainProcess':
    from level import load_levels
    from heuristic import *
    from testsuite import full_test

    easy = load_levels('levels/easy')
    medium = load_levels('levels/medium')
    hard = load_levels('levels/hard')
    insane = load_levels('levels/insane')

    full_test(easy,
              20000,
              modes=['BF', 'DF'],
              loop_checks=[False, True],
              randomises=[False, True],
              costfs=[None, cost],
              heuristicfs=[None, heuristic_optimized],
              file_name='easy')

    full_test(medium,
              20000,
              modes=['BF', 'DF'],
              loop_checks=[False, True],
              randomises=[False, True],
              costfs=[None, cost],
              heuristicfs=[None, heuristic_optimized],
              file_name='medium')

    full_test(hard,
              20000,
              modes=['BF', 'DF'],
              loop_checks=[False, True],
              randomises=[False, True],
              costfs=[None, cost],
              heuristicfs=[None, heuristic_optimized],
              file_name='hard')

    full_test(insane,
              20000,
              modes=['BF', 'DF'],
              loop_checks=[False, True],
              randomises=[False, True],
              costfs=[None, cost],
              heuristicfs=[None, heuristic_optimized],
              file_name='insane')

    full_test(insane,
              50000,
              modes=['BF'],
              loop_checks=[True],
              randomises=[False],
              costfs=[cost],
              heuristicfs=[heuristic_optimized],
              file_name='insane')
