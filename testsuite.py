from bbmodcache.bbSearch import search
import multiprocessing
from heuristic import cost
from multiprocessing import current_process

if current_process().name == 'MainProcess':
    import pandas as pd


    def full_test(states, max_nodes, count_per_thread=1, modes=('BF', 'DF'),
                  loop_checks=(False, True), randomises=(False, True), costfs=(None, cost),
                  heuristicfs=[None], print_failed=True, file_name='result'):
        df = pd.DataFrame()

        for mode in modes:
            for loop_check in loop_checks:
                for randomise in randomises:
                    for costf in costfs:
                        for heuristicf in heuristicfs:
                            result = test_algorithm(states, count_per_thread=count_per_thread, mode=mode,
                                                    max_nodes=max_nodes, loop_check=loop_check, randomise=randomise,
                                                    cost=costf, heuristic=heuristicf)

                            if print_failed and len(result['failed_indices']) > 0:
                                print('Failed indices during test:', result['failed_indices'])
                            result.pop('failed_indices')
                            df = df.append(result, ignore_index=True)

        df = df.set_index(['mode', 'loop_check', 'randomise', 'cost_used', 'heuristic_used'])
        result_filename = 'results/' + file_name + ('.html' if '.html' not in file_name else '')

        html = r'<link rel="stylesheet" type="text/css" media="screen" href="../table-style.css" />' + '\n'
        html += df.to_html()

        with open(result_filename, 'w+') as f:
            f.write(html)


def test_algorithm(test_states, count_per_thread=1, mode='BF', max_nodes=100000, loop_check=False, randomise=False, cost=None,
                   heuristic=None):
    with multiprocessing.Pool() as p:
        results = p.starmap(search, ((state, mode, max_nodes, loop_check, randomise, cost, heuristic, False,
                                      False, False, True, False) for state in test_states), count_per_thread)

    err_counter = sum([0 if result['result']['termination_condition'] == 'GOAL_STATE_FOUND' else 1 for result in results])
    success_counter = len(results) - err_counter

    success_eff = [result['result']['path_length'] for result in results if
                   result['result']['termination_condition'] == 'GOAL_STATE_FOUND']

    print('.', end='')
    return {
        'mode': mode,
        'loop_check': str(loop_check),
        'randomise': str(randomise),
        'cost_used': 'Yes' if cost is not None else 'No',
        'heuristic_used': heuristic.__name__ if heuristic is not None else 'None',
        'time': sum([result['search_stats']['time_taken'] for result in results]) / len(results),
        'memory': sum([result['search_stats']['nodes_generated'] for result in results]) / len(results),
        'solution_efficiency': sum(success_eff) / len(success_eff) if success_counter >= 0.5 * len(
            test_states) else 'ERROR',
        'successful_runs': success_counter,
        'failed_runs': err_counter,
        'failed_indices': [i for i, x in enumerate(results) if
                           x['result']['termination_condition'] != 'GOAL_STATE_FOUND']
    }
